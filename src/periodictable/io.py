# Data is downloaded from PubChem's periodic table of elements at
# https://pubchem.ncbi.nlm.nih.gov/periodic-table/#view=table
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/periodictable/CSV/?response_type=display

import json
from pathlib import Path


def read_json(path):
    """
    Reads json to dicitonary.
    """
    with open(path) as f:
        elements = json.load(f)
    return elements
