import json
import logging 
import re
from . import io, constants, pubchem
from pathlib import Path

class PeriodicTable:

    def __init__(self, jsonpath = None):
        # Load elements data
        if jsonpath is not None:
            elements = io.read_json(jsonpath)
        else:
            elements = pubchem.elements
        # Set elements as class attributes
        for Z in elements:
            element = Element(**elements.get(Z))
            setattr(self, element.symbol.lower(), element)
        return

    def __getitem__(self, Z):
        elements = vars(self)
        return elements[Z]
    
    def __iter__(self):
        for key, element in vars(self).items():
            yield element

    def atomicnumber(self, Z):
        """
        Get an element by the atomic number.
        """
        for element in self:
            if element.atomicnumber == Z:
                return element
        raise ValueError("Unknown element with the atomic number " + str(Z))

    def name(self, name):
        """
        Get an element by the name.
        """
        for element in self:
            if element.name.lower() == name:
                return element
        raise ValueError("Unknown element with the name " + str(name))

    def symbol(self, symbol):
        """
        Get an element by the symbol.
        """
        try:
            element = getattr(self, symbol.lower())  
            # Check that the attribute is an Element and not, say, a method
            if isinstance(element, Element):
                return element
            raise ValueError("Unknown element with the symbol " + str(symbol))
        except AttributeError:
            raise
    
    @property
    def elements(self):
        """
        Get all elements in the periodic table. Is equivalent to 
        vars(self).
        """
        return vars(self)
    
    def print_summary(self):
        """
        Prints all elements and a summry of their properties.
        """
        s = "{:<4} {:<7} {:<14} {:<12}"
        for i, element in enumerate(self):
            if i % 20 == 0:
                print("---------------------------------------")
                print(s.format("Z", "symbol", "name", "atomic mass"))
                print("---------------------------------------")
            print(s.format(element.atomicnumber, element.symbol, element.name, element.atomicmass))
        return


class Element:

    def __init__(self, atomicnumber, **kwargs):
        self.atomicnumber = atomicnumber
        for key in kwargs:
            setattr(self, key, kwargs.get(key))
        return
    
    def properties(self):
        """Get the properties of self."""
        # Get all except callable attributes
        noncallable_attrs = [a for a in dir(self) if not a.startswith("__") and not callable(getattr(self, a))]
        return {a: getattr(self, a) for a in noncallable_attrs}

    def print(self):
        """
        Prints all Element attributes as a human readable table.
        """
        attributes = self.properties()
        # Calculate column widths
        attr_col_width = 0
        val_col_width = 0
        for attr in attributes:
            val = attributes[attr]
            attr_col_width = len(str(attr)) if len(str(attr)) > attr_col_width else attr_col_width
            val_col_width = len(str(val)) if len(str(val)) > val_col_width else val_col_width
        # Add extra padding to the column widths
        attr_col_width += 2
        val_col_width += 2
        # String template specifying column widths
        template = "| {:<" + str(attr_col_width) + "}|{:^" + str(val_col_width) + "} |"
        # String for the top and bottom rows
        tot_widht = attr_col_width + val_col_width + 3  # +3 because there are three pipe characters "|" in template
        top_bottom_row = "+" + "-" * tot_widht + "+"
        # Print it
        print(top_bottom_row)
        for attr in attributes:
            print(template.format(attr, str(attributes[attr])))
        print(top_bottom_row)
        return

    @property
    def nvalenceelectrons(self):
        """
        Calculates and returns the number of valence electrons.
        """
        try:
            valence = self.valenceelectrons
        except AttributeError:
            raise
        # Remove "[He]" or similar from the beginning of the string
        valence = re.sub(pattern=r"^\[.*\]", repl="", string=valence)
        valence = valence.split(" ")
        for i, orbital in enumerate(valence):
            # Remove first digits and letter, like "3s" or similar
            valence[i] = int(re.sub(pattern=r"^\d*.", repl="", string=orbital))
        # Total number of valence electrons
        nvalence = sum(valence)
        return nvalence


def add_elements_to_namespace(table, namespace):
    """
    Adds variables for each Element in table to the given namespace. 
    This helps enable usage like `from periodictable import hydrogen`.

    Args:
        table -- A PeriodicTable object.
        namespace -- The namespace to which the Elements of table should
            be inserted. 
    Returns:
        A list of variable names that have been added to the specified
        namespace.

    Example usage: 
        The variable `__all__` is a list of public objects of a 
        module. When you execute 

            __all__ += add_elements_to_namespace(some_table, globals())
        
        from the `__init__.py` file, all the elements of the table 
        (hydrogen, helium, ...) will be added to the global name space. 
        This allows convenient imports like:

            from periodictable import oxygen
    """
    new_namespace_names = []
    for element in table:
        # Add all the Element (hydrogen, helium, ...) to namespace.
        namespace[element.name.lower()] = element
        # Append the keys to the list of added names
        new_namespace_names.append(element.name.lower())
    return new_namespace_names


DEFAULT_TABLE = PeriodicTable()