# Data is downloaded from PubChem's periodic table of elements at
# https://pubchem.ncbi.nlm.nih.gov/periodic-table/#view=table
# https://pubchem.ncbi.nlm.nih.gov/rest/pug/periodictable/CSV/?response_type=display

import json
import pandas as pd
from pathlib import Path
import constants


def pubchem_csv_to_dict(csvpath, outpath):
    """
    Converts the PubChem periodic table from csv form to a dictionary 
    and saves it to outpath. 

    Args:
        csvpath -- The path to the PubChem periodic table in csv format.
        outpath -- The path to where the json file will be saved.
    """
    # Read csv
    elements = pd.read_csv(csvpath, sep=",")
    # Column headers to lowercase
    elements.columns = [c.lower() for c in elements.columns]
    # increment index by 1 to match element numbers
    elements.index += 1
    # Export to dicitonary
    elements = elements.to_dict(orient="index")
    # Serializing json 
    elements = json.dumps(elements, indent = 4)
    # Write to file
    with open(outpath, "w") as f:
        elements = elements.replace("NaN", "None")
        f.write("elements = ")
        f.write(elements)
    return



if __name__ == "__main__":
    # Run this file in order to export the PubChemElements_all.csv
    # to the pubchem.py.

    # Path to csv file with periodic table data
    pubchem_csv = Path().joinpath(constants.PUBCHEM_CSV_RELATIVE_PATH).resolve()
    # Path where the resulting .py file is saved
    outfile_py = Path().joinpath(constants.PUBCHEM_PY_RELATIVE_PATH).resolve()
    # Convert a csv to a python dictionary and save to outfile_py
    pubchem_csv_to_dict(pubchem_csv, outfile_py)
