This is a list of sources from where the data is retrieved from.

* **pubchem.py**, This file is constructed from **PubChemElements_and_Wikipedia.csv**, which in turn is all the files listed below combined into one large file.

  * **PubChemElements_original.csv**, [https://pubchem.ncbi.nlm.nih.gov/rest/pug/periodictable/JSON/?response_type=save&response_basename=PubChemElements_all](https://pubchem.ncbi.nlm.nih.gov/rest/pug/periodictable/JSON/?response_type=save&response_basename=PubChemElements_all)

  * **Wikipedia_AtomicRadii.csv**, [https://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_%28data_page%29](https://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_%28data_page%29)

  * **Wikipedia_CovalentRadii.csv**, [https://en.wikipedia.org/wiki/Covalent_radius](https://en.wikipedia.org/wiki/Covalent_radius), The values in the table are based on a statistical analysis of more than 228,000 experimental bond lengths from the Cambridge Structural Database. For carbon, values are given for the different hybridisations of the orbitals. **IMPORTANT:** In the cases where multiple radii are given at Wikipedia, the largest value is used here in this package.