from . import elements
from .elements import PeriodicTable

# Enable usage like from periodictable import elements, hydrogen, helium, ...
table = elements.DEFAULT_TABLE
__all__ = ['table']
__all__ += elements.add_elements_to_namespace(table, globals())
