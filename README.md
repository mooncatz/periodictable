[[_TOC_]]
# PubChem Periodic Table of Elements
A simple periodic table of elements Python package with data from [PubChem](https://pubchem.ncbi.nlm.nih.gov/periodic-table/#view=table) and [Wikipedia](https://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_%28data_page%29). 

> [PubChem](https://pubchem.ncbi.nlm.nih.gov/) is an open chemistry database at the National Institutes of Health (NIH). [1] The data can also be manually inspected in the file [`periodictable/elements.json`](periodictable/PubChemElements_all.json). 

>**Disclaimer:** This package is not provided by PubChem and the author is not associated with PubChem. This package only uses PubChem's open data. 

>**References:**  
> [1] National Center for Biotechnology Information (2022). PubChem Periodic Table of Elements. Retrieved May 19, 2022 from https://pubchem.ncbi.nlm.nih.gov/periodic-table/.

# Installation

Prebuilt libraries are located in the [/dist](/dist) directory and an be installed with:

```bash
# Using pip
python3 -m pip install dist/periodictable-X.X.X-py3-none-any.whl

# Using pipenv
pipenv install dist/periodictable-X.X.X-py3-none-any.whl
```

You can also build it yourself:

```bash
python3 -m build
``` 

# Basic usage

```python
>>> from periodictable import table

# Access individual elements in the table
>>> table.h
<src.periodictable.elements.Element object at 0x7f5a43d85580>

# Access a property (e.g. name or electronegativity) of an element
>>> table.h.name
'Hydrogen'
>>> table.h.electronegativity
2.2

# All available element properties as a dictionary 
>>> table.au.properties()
{"atomicnumber": 79,
 "symbol": "Au",
 "name": "Gold",
 "atomicmass": 196.96657,
 "cpkhexcolor": "FFD123",
 "electronconfiguration": "[Xe]6s1 4f14 5d10",
 "electronegativity": 2.54,
 "atomicradius": 166.0,
 "ionizationenergy": 9.226,
 "electronaffinity": 2.309,
 "oxidationstates": "+3, +1",
 "standardstate": "Solid",
 "meltingpoint": 1337.33,
 "boilingpoint": 3129.0,
 "density": 19.282,
 "groupblock": "Transition metal",
 "yeardiscovered": 9999,
 "valenceelectrons": "5d10 6s1",
 "nvalenceelectrons": 11,
 "empiricalradius": 150.0,
 "calculatedradius": 171.0,
 "covalentradiussinglebond": 133.0,
 "covalentradiustriplebond": None,
 "metallicradius": 151.0}

# Get an element by atomic number
>>> c = table.atomicnumber(6)

# Get an element by name
>>> he = table.name("helium")

# Get an element by symbol
>>> o = table.symbol("O")
```

Import elements directly

```python
# Import individual elements directly
>>> from periodictable import hydrogen
>>> hydrogen.electronegativity
2.0
```

Print in human readable format

```python
# Element properties in human readable format
>>> table.au.print()
+------------------------------------------------+
| atomicnumber              |        79          |
| symbol                    |        Au          |
| name                      |       Gold         |
| atomicmass                |     196.96657      |
| cpkhexcolor               |      FFD123        |
| electronconfiguration     | [Xe]6s1 4f14 5d10  |
| electronegativity         |       2.54         |
| atomicradius              |       166.0        |
| ionizationenergy          |       9.226        |
| electronaffinity          |       2.309        |
| oxidationstates           |      +3, +1        |
| standardstate             |       Solid        |
| meltingpoint              |      1337.33       |
| boilingpoint              |      3129.0        |
| density                   |      19.282        |
| groupblock                | Transition metal   |
| yeardiscovered            |       9999         |
| valenceelectrons          |     5d10 6s1       |
| empiricalradius           |       150.0        |
| calculatedradius          |       171.0        |
| covalentradiussinglebond  |       133.0        |
| covalentradiustriplebond  |       None         |
| metallicradius            |       151.0        |
+------------------------------------------------+

# Table summary
>>> table.print_summary()
---------------------------------------
Z    symbol  name           atomic mass 
---------------------------------------
1    H       Hydrogen       1.008       
2    He      Helium         4.0026      
3    Li      Lithium        7.0         
4    Be      Beryllium      9.012183    
5    B       Boron          10.81       
6    C       Carbon         12.011   
```

All table elements can be accessed with
```python
# All table Element objects
>>> table.elements
{'h': <periodictable.elements.Element object at 0x10bb81910>, 
 'he': <periodictable.elements.Element object at 0x10bb97550>, 
 'li': <periodictable.elements.Element object at 0x10bb97c40>, 
 ...}
```

# Advanced usage

## The PeriodicTable object

There are two main classes, `periodictable.core.PeriodicTable` and `periodictable.core.Elements`. When initialized, the `PeriodicTable` loads the file `pubchem.py`. For each element in the `elements` dictionary, an `Element` object is instantiated and added as an attribute to the `PeriodicTable`. This results in the `PeriodicTable` having all the elements as its attributes. Here follows an example.

```python
>>> from periodictable import PeriodicTable
>>>
>>> # When ptable is instantiated, all elements added as attributes
>>> ptable = PeriodicTable()
>>>
>>> # Access the hydrogen Element 
>>> ptable.h
<periodictable.core.Element object at 0x11019fa60>
```

For convenience, when the `periodictable` package is imported, a default `PeriodicTable` object is instantiated with the variable name `table`. That is why one can directly import the `table` without the need to manually instatiate a `PeriodicTable` object (see `__init__.py`). That is:

```python
>>> # A ready-to-use periodic table can be directly imported
>>> from periodictable import table
>>> table
>>>
>>> # Or
>>> import periodictable
>>> periodictable.table
>>>
>>> # This is unnecissary to do
>>> from periodictable import PeriodicTable
>>> table = PeriodicTable()
```

## Using custom data

To use the `PeriodicTable` with custom data, simply call it with the path to a 
json file.

```python
>>> # Load custom data
>>> from periodictable import PeriodicTable
>>> custom_table = PeriodicTable(jsonpath="path/to/custom/data.json")
``` 

## The Element object

Each element is an `Element` object. The elements of `table` are added to the global namespace when the `periodictable` package is being imported. This enables the `Elements` of the default `table` to be directly imported. Like this:

```python
>>> # The elements of the default table can be imported directly
>>> from periodictable import polonium
>>> polonium.ionizationenergy
8.417
>>>
>>> # The imported polonium object is just an attribute of the default table
>>> from periodictable import table, polonium
>>>
>>> table.po == polonium
True
>>>
>>> table.po
<periodictable.core.Element object at 0x11033f910>
>>>
>>> polonium
<periodictable.core.Element object at 0x11033f910>
```

# Docs

Here is a list of all properties and methods of the `Table` and `Element` objects.

## `Table`

Represents the periodic table. 

When initialized, all elements are added as `Element` objects to the `Table`'s properties. This enables usage like `table.element_symbol` for accessing elements in the table. 

### `Table.atomicnumber(Z)`  

Get an element by the atomic number.  

```python
>>> e = table.atomicnumber(2)
>>> e.name
'Helium'
```

### `Table.name(name)`

Get an element by the name.

```python
>>> e = table.name('helium')
>>> e.name
'Helium'
```

### `Table.symbol(symbol)`

Get an element by the symbol.

```python
>>> e = table.symbol('he')
>>> e.name
'Helium'
```

### `Table.elements`

A `@property` decorated method that returns a dictionary of all elements (`Element` objects) in the periodic table. Is equivalent to `vars(self)`.

```python
>>> table.elements
{'h': <periodictable.elements.Element object at 0x7f6713c5d120>, 
'he': <periodictable.elements.Element object at 0x7f6713c88460>, 
'li': <periodictable.elements.Element object at 0x7f6713c88c70>, 
... }
```

### `Table.print_summary()`

Prints a human readable summary of all elements.

```python
>>> table.print_summary()
---------------------------------------
Z    symbol  name           atomic mass 
---------------------------------------
1    H       Hydrogen       1.008       
2    He      Helium         4.0026      
3    Li      Lithium        7.0        
```



## `Element`

Reperesents an individual element in the periodic table and holds the element's data. 

### `Element.properties()`

Returns a dictionary of all element properties, as would be returned by `vars(self)`, plus all methods decorated with the `@property` decorator.

```python
>>> table.au.properties
{"atomicnumber": 79,
 "symbol": "Au",
 "name": "Gold",
 "atomicmass": 196.96657,
 "cpkhexcolor": "FFD123",
 "electronconfiguration": "[Xe]6s1 4f14 5d10",
 "electronegativity": 2.54,
 "atomicradius": 166.0,
 "ionizationenergy": 9.226,
 "electronaffinity": 2.309,
 "oxidationstates": "+3, +1",
 "standardstate": "Solid",
 "meltingpoint": 1337.33,
 "boilingpoint": 3129.0,
 "density": 19.282,
 "groupblock": "Transition metal",
 "yeardiscovered": 9999,
 "valenceelectrons": "5d10 6s1",
 "nvalenceelectrons": 11,
 "empiricalradius": 150.0,
 "calculatedradius": 171.0,
 "covalentradiussinglebond": 133.0,
 "covalentradiustriplebond": None,
 "metallicradius": 151.0}
 ```

### `Element.print()`

Prints `Element.properties()` in human readable format.

### `Elements.nvalenceelectrons`

A `@property` decorated method that calculates and return the number of valence electrons. The number of valence electrons is calculated based on `Element.valenceelectrons`
